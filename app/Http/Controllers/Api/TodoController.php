<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\TodoRequest;
use Illuminate\Http\Request;
use App\Http\Resources\TodoResource;
use App\Events\TodoCreatedEvent;
use Illuminate\Support\Facades\Auth;
use App\Models\Todo;


class TodoController extends Controller
{
  public function index()
  {

    $todos = Todo::where('user_id', Auth::user()->id)->orderBy('created_at', 'desc')->get();

    return TodoResource::collection($todos);
  }

  public function store(TodoRequest $request)
  {

    $todo = Auth::user()->todos()->create([
      'text' => $request->text,
      'done' => 0
    ]);

    event(new TodoCreatedEvent($todo));

    return new TodoResource($todo);
  }

  public function delete($id)
  {
    $todo = Todo::find($id);
    if (Auth::id() == $todo->id) {
      $todo->delete();
      return 'success';
    }
    return response()->json(['message' => 'Youre Not Authorized']);
  }

  public function changeDoneStatus($id)
  {
    $todo = Todo::find($id);
    if ($todo->done == 1) {
      $update = 0;
    } else {
      $update = 1;
    }

    if (Auth::id() == $todo->id) {
      $todo->update([
        'done' => $update
      ]);

      return new TodoResource($todo);
    }

    return response()->json(['message' => 'Youre Not Authorized']);
  }
}
