<?php

namespace App\Listeners;

use App\Events\TodoCreatedEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Mail\TodoCreatedMail;
use App\Models\Todo;
use Illuminate\Support\Facades\Mail;

class SendEmailNotification implements ShouldQueue
{
  public $todo;
  /**
   * Create the event listener.
   *
   * @return void
   */
  public function __construct(Todo $todo)
  {
    $this->todo = $todo;
  }

  /**
   * Handle the event.
   *
   * @param  TodoCreatedEvent  $event
   * @return void
   */
  public function handle(TodoCreatedEvent $event)
  {
    Mail::to($event->todo->user)->send(new TodoCreatedMail($event->todo));
  }
}
